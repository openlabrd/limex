#!/usr/bin/env python
# -*- coding: utf-8 -*-

import 	tkinter 	as tk
from 	tkinter 	import 	ttk, filedialog, StringVar, Menu, VERTICAL, INSERT, TclError


text_area_bg_color 	= '#%02x%02x%02x' % (255, 255, 255)
text_color 			= '#%02x%02x%02x' % (129, 135, 108)

PROGRAM_NAME ='Limex'

class MenuBar(tk.Frame):
	def __init__(self, parent, name_cascades_options_commands, *args, **kwargs):
		tk.Frame.__init__(self, parent, *args, **kwargs)
		self.parent = parent

		if name_cascades_options_commands:
			for menu_name, cascades_options_commands in name_cascades_options_commands.items():
				self.menu_bar = Menu(self, name= menu_name)

				for cascade in cascades_options_commands:
					self.new_cascade = Menu(self.menu_bar, name= cascade.lower()+'<root_menu_bar_cascades>', tearoff=0)
					self.menu_bar.add_cascade(label= cascade, menu= self.new_cascade)

					for option_and_command in cascades_options_commands[cascade]:
						for option, command in option_and_command.items():
							self.new_cascade.add_command(label= option, command= command)


class StatusBar(tk.Frame):
	def __init__(self, parent, *args, **kwargs):
		tk.Frame.__init__(self, parent, *args, **kwargs)
		self.configure(bd=3)
		
		self.parent = parent

		self.info_label = tk.Label (self, textvariable=cursor_position_info_var)
		self.info_label.pack(side='left')
		
		self.text_selection_info = tk.Label (self, textvariable=text_selection_info_var)
		self.text_selection_info.pack(side='left', padx=15)

		self.text_style = ttk.Combobox(self, textvariable=text_style_var, values=["Plain text", "Python"])
		self.text_style.bind("<<ComboboxSelected>>", self.text_style_action)
		self.text_style.pack(side='right')
		self.text_style.current(0)

	def text_style_action(self, event):
		selection=(text_style_var.get())
		print(selection)


class TextArea(tk.Frame):
	# Witgets ---------------------------------------------------------------------------------
	def __init__(self, parent, *args, **kwargs):
		tk.Frame.__init__(self, parent, *args, **kwargs)
		self.parent = parent
		self.StatusBar = StatusBar(self)

		# Scrollbar vertical que afecta a las dos Text Box
		self.yscrollbar = ttk.Scrollbar(self, orient='vertical', command=self.yview)


		# Scrollbar horizontal que afecta solo al text box principal
		self.xscrollbar = ttk.Scrollbar(self, orient='h')
		self.xscrollbar.pack(side='bottom', fill='x')
		self.xscrollbar.pack_forget()


		# Text Box que muestra el numero de cada linea --------------------\
		self.line_nomber = tk.Text	(
									self, 
									state='disabled', 
									highlightthickness = 0, 
									bd = 0, 
									takefocus=0, 
									wrap='none', 
									width=2, 
									bg='gray80', 
									fg=text_color, 
									yscrollcommand=self.yscroll1
								)

		self.line_nomber.pack	(
									fill='y', 
									side='left'
								)
		#-------------------------------------------------------------------/


		#-------------------------------------------------\
		self.sep = ttk.Separator(self, orient=VERTICAL)	#-------------------------------------------------
		self.sep.pack(fill='y', side='left', padx=(5,0))#-------------------------------------------------
		#-------------------------------------------------/


		# Text Box principal ------------------------------------------------\
		self.text_content = tk.Text(
									self, 
									undo=1, 
									fg=text_color, 
									bg=text_area_bg_color,
									highlightthickness = 0, 
									bd = 0, 
									yscrollcommand=self.yscroll2, 
									xscrollcommand=self.xscrollbar.set
								)


		self.text_content.pack(expand=1, fill='both', side='left')
		self.text_content.bind('<Any-KeyRelease>', self.on_content_changed)
		self.text_content.bind('<ButtonRelease>', self.on_mouse_boton1)
		self.text_content.focus_set()
		self.xscrollbar.config(command=self.text_content.xview)
		#-------------------------------------------------------------------/


	def on_content_changed(self, event):
		# ~ self.StatusBar.text_selection_info.configure(text="sel: 0 col. 0 char.")
		text_selection_info_var.set("sel: 0 col. 0 char.")
		self.update_line_numbers()
		self.update_cursor_info_bar()

		input_ 							= event.keysym 					#Letra o tecla que fue pulsada.
		cursor_position 				= event.widget.index(INSERT) 		#Posicion actual del cursor. formato: '2.3' = fila dos tercer caracter.
		cursor_row, cursor_col 			= event.widget.index(INSERT).split('.') # Se obtienen por separado la fila y columna del cursor.
		cursor_left_text 				= event.widget.get('current linestart', cursor_position)
		cursor_right_text 				= event.widget.get(cursor_position, 'current lineend')
		cursor_left_last_tow_rows_text 	= event.widget.get("{}.0".format(int(cursor_row)-1), cursor_position)
		# ~ print(input_)

		if text_style_var.get() == 'Python':
			event.widget.configure(wrap='none')

			if input_ == 'parenleft':
				if 'def' in cursor_left_text:
					event.widget.insert(cursor_position, '):')

				else:
					event.widget.insert(cursor_position, ')')
					event.widget.mark_set("insert", cursor_position)


			elif 'def' in cursor_left_text and cursor_left_text.count(' ') >1 and '(' not in cursor_left_text:
				if input_ == 'space':

					event.widget.delete("{}.{}".format(cursor_row, int(cursor_col)-1), cursor_position)
					event.widget.insert("{}.{}".format(cursor_row, int(cursor_col)-1), '_')


			elif input_ == 'Return':
				
				try:
					if cursor_left_last_tow_rows_text[-2] == ':':
						event.widget.insert(cursor_position, '\t')
				except IndexError:
					pass

		
		if text_style_var.get() == 'Plain text':
			event.widget.configure(wrap='word')


		try:#Si hay texto seleccionado se actualizara en status bar.
			selected_text = event.widget.get(tk.SEL_FIRST, tk.SEL_LAST)
			self.selected_charatets_update(selected_text)
		except TclError:
			pass


		if self.text_content.xview() == (0.0, 1.0):
			self.xscrollbar.pack_forget()
		else:
			self.xscrollbar.pack()


	def update_line_numbers(self, event=None):
		row, col = self.text_content.index("end").split('.')
		row = int(row) - 1

		from_line_nomber_text = self.line_nomber.get('1.0', 'end-1c')


		if str(row) in from_line_nomber_text:
			if str(row+1) in from_line_nomber_text:
				self.line_nomber.config(state='normal')
				self.line_nomber.delete("%d.%d" % (row+1,0),  'end')
				self.line_nomber.config(state='disabled')
		
		else:
			row = str(row) + '\n'
			self.line_nomber.config(state='normal')
			self.line_nomber.insert('end', row)
			self.line_nomber.config(state='disabled')


	def update_cursor_info_bar(self, event=None):
		total_columns = self.text_content.index('end-1c').split('.')[0]
		row, col = self.text_content.index(INSERT).split('.')
		line_num, col_num = str(int(row)), str(int(col) + 1)
		infotext = "Line: {} / {} Col: {}".format(line_num, total_columns, col_num)
		#self.StatusBar.info_label.configure(text=infotext)
		cursor_position_info_var.set(infotext)



	def selected_charatets_update(self,selected_text):
		selected_text_line_breaks = selected_text.count('\n') + 1
		selected_text_characters = len(selected_text) 

		selected_text_info = "sel: {} col. {} char.".format(selected_text_line_breaks, selected_text_characters)
		#self.StatusBar.text_selection_info.config(text=selected_text_info)
		text_selection_info_var.set(selected_text_info)


	def on_mouse_boton1(self, event):
		try:
			selected_text = event.widget.get(tk.SEL_FIRST, tk.SEL_LAST)
			self.selected_charatets_update(selected_text)
		except TclError:
			pass


	# Funciones del scroll compartido ----------------------
	def yscroll1(self, *args):

		if self.text_content.yview() != self.line_nomber.yview():
			self.text_content.yview_moveto(args[0])
			
			if self.text_content.yview() == (0.0, 1.0):
				self.yscrollbar.pack_forget()
			else:
				self.yscrollbar.pack(side='right', fill='y', pady=(0,0))



		self.yscrollbar.set(*args)

	def yscroll2(self, *args):
		if self.line_nomber.yview() != self.text_content.yview():
			self.line_nomber.yview_moveto(args[0])
		self.yscrollbar.set(*args)

	def yview(self, *args):

		self.line_nomber.yview(*args)
		self.text_content.yview(*args)


class Manager():
	def __init__(self, root, *args, **kwargs):
		self.parent = root
		self.parent.title(PROGRAM_NAME)

		cascades_options_commands ={	'root_menu_bar':{
															'File':	(
																		{'New':self.menu_new_action},
																		{'Open':self.menu_open_action},
																		{'Save':self.menu_save_action},
																		{'Save as':self.menu_save_as_action},
																		{'Exit':self.menu_exit_action}
																	),

															'Edit':(
																		{'Cut':self.menu_cut_action},
																		{'Copy':self.menu_copy_action},
																		{'Paste':self.menu_paste_action},
																		{'Duplicate Line':self.menu_duplicate_line_action},
																		{'Preferences':self.menu_preferences_action}
																	),

															'Help':	(
																		{'About':self.menu_about_action},
																	)
														}
									}

		self.MenuBar = MenuBar(self.parent, cascades_options_commands)
		self.parent.config(menu=self.MenuBar.menu_bar)
		
		self.StatusBar = StatusBar(self.parent)
		self.StatusBar.pack(side="bottom", fill="x")
		
		self.TextArea = TextArea(self.parent)
		self.TextArea.pack(fill="both", expand=True)


	#------------------------------\
	def menu_new_action(self):
		print('menu cliked')


	def menu_open_action(self):
		print('menu cliked')


	def menu_save_action(self):
		print('menu cliked')


	def menu_save_as_action(self):
		input_file_name = tk.filedialog.asksaveasfilename	(
																defaultextension=".txt",
																filetypes=	[
																				(	
																					"Python", 
																					"*.py"), 
																					
																				(
																					"All Files", 
																					"*.*"), 
																				
																				(
																					"Text Documents", 
																					"*.txt")
																			]
															)
		return "break"


	def menu_exit_action(self):
		print('menu cliked')


	def menu_cut_action(self):
		print('menu cliked')


	def menu_copy_action(self):
		print('menu cliked')


	def menu_paste_action(self):
		print('menu cliked')


	def menu_duplicate_line_action(self):
		print('menu cliked')


	def menu_preferences_action(self):
		print('menu cliked')


	def menu_about_action(self):
		print('menu cliked')



if __name__ == "__main__":
	root = tk.Tk()
	
	cursor_position_info_var = StringVar()
	text_style_var = StringVar()
	text_selection_info_var = StringVar()
	Manager(root)

	root.mainloop()


